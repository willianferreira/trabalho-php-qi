<?php

include 'Conexao.php';

class EmpresaModel {

    private $conexao = null;

    function __construct() {

        $this->conexao = Conexao::getInstancia();
    }

    function cadastrarEmpresa($f) {
        try {
            $sql = $this->conexao->prepare("insert into filial"
                    . "(idfilial,nome,login,senha,cidade)"
                    . " values(null,?,?,?,?)");
            $sql->bindValue(1, $f->nome);
            $sql->bindValue(2, $f->login);
            $sql->bindValue(3, $f->senha);
            $sql->bindValue(4, $f->cidade);
            $sql->execute();
            $this->conexao = null;
        } catch (Exception $e) {
            echo "Erro ao Inserir!";
        }
    }

    function BuscarEmpresa() {
        try {
            $sql = $this->conexao->query('select * from filial ');
            $array = array();
            $array = $sql->fetchAll(PDO::FETCH_CLASS, "Empresa");
            $this->conexao = null;
            return $array;
        } catch (Exception $e) {
            echo "Erro ao buscar!";
        }
    }

    function filtrarEmpresa($query) {
        try {
            $sql = $this->conexao->query('select '
                    . '* from filial ' . $query);
            $array = array();
            $array = $sql->fetchAll(PDO::FETCH_CLASS, "Empresa");
            $this->conexao = null;
            return $array;
        } catch (Exception $e) {
            echo "erro ao buscar!";
        }
    }

    function deletarEmpresa($id) {
        try {

            $sql = $this->conexao->prepare("delete from "
                    . "filial where idfilial=?");
            $sql->bindValue(1, $id);
            $sql->execute();
            $this->conexao = null;
        } catch (Exception $e) {
            echo "Erro ao Inserir!";
        }
    }

    function alterarEmpresa($f) {
        try {
            $sql = $this->conexao->prepare("update "
                    . "filial set login=?,"
                    . "senha=?,"
                    . "cidade=? "
                    . "where "
                    . "idfilial=?");
            $sql->bindValue(1, $u->login);
            $sql->bindValue(2, $u->senha);
            $sql->bindValue(3, $u->cidade);
            $sql->bindValue(4, $u->idfilial);
            $sql->execute();
            $this->conexao = null;
        } catch (Exception $e) {
            echo "Erro ao Inserir!";
        }
    }

}



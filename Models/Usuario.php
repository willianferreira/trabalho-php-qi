<?php

class Usuario {
    private $idcliente;
    private $nome;
    private $login;
    private $senha;
    private $tel;
    private $cpf;
    
       public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name=$value;
    }

    public function __toString() {
        return "<h1>Usuário: ".$this->idcliente."</h1>".
                "<br/>Nome: ".$this->nome.
                "<br/>Login: ".$this->login.
                "<br/>Senha: ".$this->senha.
                "<br/>Telefone: ".$this->tel.
                "<br/>CPF: ".$this->cpf;
                
}

}


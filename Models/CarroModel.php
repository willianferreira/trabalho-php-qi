<?php

include 'Conexao.php';

class Carro {

    private $conexao = null;

    function __construct() {

        $this->conexao = Conexao::getInstancia();
    }

    function cadastrarCarro($r) {

        try {
            $sql = $this->conexao->prepare("insert into carro"
                    . "(idcarro,modelo,marca,placa,filial,preco)"
                    . " values(null,?,?,?,?,?)");
            $sql->bindValue(1, $r->modelo);
            $sql->bindValue(2, $r->marca);
            $sql->bindValue(3, $r->placa);
            $sql->bindValue(4, $r->filial);
            $sql->bindValue(5, $r->preco);
            $sql->execute();
            $this->conexao = null;
        } catch (Exception $e) {
            echo "Erro ao Inserir!";
        }
    }

    function selecionarCarro() {
        try {
            $sql = $this->conexao->query('select * from carro');
            $array = array();
            $array = $sql->fetchAll(PDO::FETCH_CLASS, "Carro");
            $this->conexao = null;
            return $array;
        } catch (Exception $e) {
            echo "Erro ao buscar!";
        }
    }

}

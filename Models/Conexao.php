<?php

class Conexao extends PDO {

    private static $instancia = null;

    function Conexao($dsn, $usuario, $senha) {
        parent::__construct($dsn, $usuario, $senha);
    }

    static function getInstancia() {
        try {
            self::$instancia = new Conexao("mysql:dbname=locasinos;host=localhost", "root", "");
        } catch (Exception $e) {
            echo "Erro ao conectar!";
            exit();
        }
        return self::$instancia;
    }

}

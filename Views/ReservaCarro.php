<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LocaSinos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../Index/css/bootstrap.min.css" type="text/css"/>
        <link rel="stylesheet" href="../Index/style.css" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../Index/js/bootstrap.min.js"></script>
    </head>
    <body>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-ml-12 col2">
                    <img src="../Index/22.png" alt="logo"/>
                </div>
            </div><br/>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h1>Nossos Carros</h1>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="armas">
                    <div class="thumbnail imagens">
                        <img src="../Index/gol.jpg" alt=""/>
                        <div class="caption">
                            <p>Volkswagen Gol 1.0 Completo</p>
                            <button data-toggle="modal" data-target="#ver1">Ver</button>
                            <div class="modal fade" id="ver1" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content conteudo">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">Volkswagen Gol 1.0 Completo</h3>
                                            <img class="produto" src="../Index/gol.jpg" alt=""/>
                                            <label class="preco">Preço: R$100,00</label>
                                            <br class="clr"/>
                                            <input class="adicionar" type="submit" name="Carrinho" value="Alugar"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="armas">
                    <div class="thumbnail imagens">
                        <img src="../Index/cobalt.png" alt=""/>
                        <div class="caption">
                            <p>Novo Cobalt 1.8 Completo</p>
                            <div>
                                <button data-toggle="modal" data-target="#ver2">Ver</button>
                                <div class="modal fade" id="ver2" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content conteudo">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h3 class="modal-title">Novo Cobalt 1.8 Completo</h3>
                                                <img class="produto" src="../Index/cobalt.png" alt=""/>
                                                
                                                <form action="ReservaHorarios.php" method="post">
                                                      <label class="preco">Preço: </label>
                                                      <input type="text" name="preco" readonly="true" value="153,30"/>
                                                       <input class="adicionar" type="submit" name="Carrinho" value="Alugar"/>
                                                </form>
                                                
                                                
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="armas">
                    <div class="thumbnail imagens">
                        <img src="../Index/hb20.png" alt=""/>
                        <div class="caption">
                            <p>Novo HB20 hatch 1.6 Completo</p>
                            <button data-toggle="modal" data-target="#ver3">Ver</button>
                            <div class="modal fade" id="ver3" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content conteudo">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">HB20 hatch 1.0 Completo</h3>
                                            <img class="produto" src="../Index/hb20.png" alt=""/>
                                            <label class="preco">Preço: R$80,25</label>
                                            <br class="clr"/>
                                            <input class="adicionar" type="submit" name="Carrinho" value="Alugar"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="armas">
                    <div class="thumbnail imagens">
                        <img src="../Index/fiesta.jpg" alt=""/>
                        <div class="caption">
                            <p>Ford New Fiesta Sedan 1.6 Completo</p>
                            <button data-toggle="modal" data-target="#ver4">Ver</button>
                            <div class="modal fade" id="ver4" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content conteudo">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">Ford New Fiesta Sedan 1.6 Completo</h3>
                                            <img class="produto" src="../Index/fiesta.jpg" alt=""/>
                                            <label class="preco">Preço: R$110,90</label>
                                            <br class="clr"/>
                                            <input class="adicionar" type="submit" name="Carrinho" value="Alugar"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <nav id="menu" class="telamin">
                <ul>
                    <li><a href="index.php">PAGINA INICIAL</a></li>
                </ul>
            </nav>
        </div>

        <div class="container-fluid col1">
            <div class="row">
                <div class="midia logos textr">
                    <h4>Siga:</h4>
                    <ul class="list-inline">
                        <li><a href="https://www.facebook.com/" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://plus.google.com" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="https://www.instagram.com/" class="icoInsta" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>


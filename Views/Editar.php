<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Editar</title>
    </head>
    <body>
        <form action="../Control/ControleEmpresa.php?op=buscaID" method="post">
            <input type="number" name="idfilial" placeholder="Digite o ID a ser alterado..."/>
            <input type="submit" value="Buscar ID">
        </form>
        <form action="../Control/ControleEmpresa.php?op=filtrar" method="post">
            <fieldset><legend>Busca Personalizada</legend>
                <input placeholder="Digite o Valor para Busca..." type="text" name="valor"/></br>
                <input type="radio" name="filtro" value="nome"/>Nome</br>
                <input type="radio" name="filtro" value="cidade"/>Tipo</br>
                <input type="radio" name="filtro" value="idfilial"/>ID</br>
                <input type="submit" value="Filtrar"/>
            </fieldset>
        </form>
        <form action="../Control/ControleEmpresa.php?op=deletar" method="post">
            <input type="number" name="idfilial" placeholder="Digite o ID a ser deletado..."/>
            <input type="submit" value="Deletar">
        </form>

        <?php
        session_start();
        include '../Model/Empresa.php';
        $dados = unserialize($_SESSION['dados']);
        foreach ($dados as $d) {
            echo $d . "<br/>";
            echo "----------------------------------------";
        }
        ?>
    </body>
</html>
